# Projet à utiliser pour la matinée de formation CI/CD

## Pré-requis

- nodejs v13

## Installer les dépendances

```sh
$ npm ci
```

## Démarrer l'application

```sh
$ npm start
```

## Build l'application

```sh
$ npm run build
```

## Exécuter le linter

```sh
$ npm run lint
```

## Exécuter les tests

```sh
$ npm run test
```
